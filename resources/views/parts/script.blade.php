<!-- Argon Scripts -->
    <!-- Core -->
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src=" {{ asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }} "></script>
    <!-- Optional JS -->
    <script src="{{ asset('vendor/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('vendor/chart.js/dist/Chart.extension.js') }}"></script>
    <!-- Argon JS -->
    <script src="{{ asset('js/argon.js') }}"></script>

    <!-- Typewriter effect -->
    <script src="{{ asset('vendor/typed/typed.js') }}"></script>

    <!-- NicescrollJS-->
    <script src="{{asset('vendor/nicescroll/jquery.nicescroll.min.js')}}"></script>

    <!-- Jquery Scroll Tabs -->
    <script src="{{asset('vendor/jquery-scroll/jquery.scrolling-tabs.min.js')}}"></script>

    <!-- Modal Fullscreen -->
    <script src="{{asset('vendor/animatedModal/animatedModal.min.js')}}"></script>

    <script>
        var typed = new Typed('#typed', {
        stringsElement: '#typed-strings',
        typeSpeed: 60,
        backSpeed: 10,
        smartBackspace: true, // this is a default
        loop: true
        });

        var typed2 = new Typed('#typed-2', {
        stringsElement: '#typed-strings-2',
        typeSpeed: 120,
        backSpeed: 10,
        smartBackspace: true, // this is a default
        loop: true
        });

        var typed3 = new Typed('#typed-3', {
        stringsElement: '#typed-strings-3',
        typeSpeed: 120,
        backSpeed: 10,
        smartBackspace: true, // this is a default
        loop: true
        });

        var typed4 = new Typed('#typed-4', {
        stringsElement: '#typed-strings-4',
        typeSpeed: 120,
        backSpeed: 10,
        smartBackspace: true, // this is a default
        loop: true
        });

        $("a.nav-link").click(function(){
        $(".navbar-collapse").removeClass("show");
        $(".navbar-collapse").addClass("hide");
        });


        $("a.link-scroll[href^='#']").on('click', function(e) {

        // prevent default anchor click behavior
        e.preventDefault();
        var target = $(this).attr('href');
        // animate
        $('html, body').animate({
            scrollTop: $(this.hash).offset().top
        }, 800, function(){

            // when done, add hash to url
            // (default click behaviour)
            window.location.hash = target;
        });
        });

        $('.nav-pills').scrollingTabs({
        bootstrapVersion: 4,
        cssClassLeftArrow: 'fa fa-chevron-left',
        cssClassRightArrow: 'fa fa-chevron-right'
        });

        $("#demo01").animatedModal({
        animatedIn:'bounceInUp',
        animatedOut:'bounceOutDown',
        color:'rgba(255,255,255,0.95)'
        });

        $(document).ready(function(){
        $(".btn-close-modal").click(function(){
            $("html").css({'overflow':'hidden'});
            $("body").css({'overflow':'hidden'});
        });
        });

        $("body").niceScroll();
        $("#about-us-scroll").niceScroll({
        autohidemode: false,
        cursorborderradius: '10px'
        });
    </script>