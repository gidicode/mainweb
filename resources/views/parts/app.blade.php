<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="We are a creative agency that will help you to solve your problem with technology">
        <meta name="author" content="Gidicode Project">
        <title>Gidicode Project - Creating an Innovation</title>
        <!-- Favicon -->
        <link href="{{ asset('img/faicon.png') }}" rel="icon" type="image/png">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700,800,900&amp;subset=devanagari,latin-ext" rel="stylesheet">
        <!-- Icons -->
        <link href="{{ asset('vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
        <link href="{{ asset('vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    
        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('css/argon.css') }}" rel="stylesheet">
    
        <!-- JQuery Tab Scroll -->
        <link rel="stylesheet" href="{{ asset('vendor/jquery-scroll/jquery.scrolling-tabs.min.css') }}">
    
        <!-- AnimateCSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.0/animate.min.css">
    
    
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131813069-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
    
            gtag('config', 'UA-131813069-1');
        </script>
    </head>

    <body data-spy="scroll" data-target="#navbar-menu" data-offset="20">
        <!-- Sidenav -->
        <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
            <div class="container-fluid">
            
            <!-- Brand -->
            <a class="navbar-brand float-left" href="{{ url('/') }}">
                <img src="{{asset('img/logo.png')}}" style="width:180px;" class="navbar-brand-img" alt="...">
            </a>
            <!-- Toggler -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Collapse header -->
                <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                    <a href="{{ url('/') }}">
                        <img src="./assets/img/logo.png">
                    </a>
                    </div>
                    <div class="col-6 collapse-close">
                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                        <span></span>
                        <span></span>
                    </button>
                    </div>
                </div>
                </div>

                <!-- Navigation -->
                <ul class="navbar-nav">
                
                <li class="nav-item">
                    <a class="nav-link btn btn-sm btn-secondary ml-4" href="./indonesia.html" style="width:80%; padding:8px">
                    <img src="{{asset('img/indonesia-flag-1.png')}}" class="mr-3 ml-0">
                    see in Bahasa 
                    <i class="fas fa-arrow-right"></i>
                    </a>
                </li>
                <div class="mt-6 content-desktop"></div>
                <div class="mt-3 content-mobile"></div>
                </ul>

                <ul class="navbar-nav" id="navbar-menu">
                <li class="nav-item"> 
                    <a class="nav-link link-scroll active" href="#start">
                    Start
                    </a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link link-scroll" href="#we-do">
                    What we do
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link link-scroll" href="#for-biz">
                    for business & startup
                    </a>
                </li>

                <!--
                <li class="nav-item">
                    <a class="nav-link link-scroll" href="#our-expert">
                    our expertise
                    </a>
                </li> -->

                <li class="nav-item">
                    <a class="nav-link link-scroll" href="#about-us">
                    About Us
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link link-scroll" href="#connect-us">
                    Connect Us
                    </a>
                </li>
                </ul>

                <div class="side-footer">
                <p>
                    &copy 2019 All Right Reserved<br>
                    <strong style="font-size:13pt; font-weight: 700">Gidicode Project</strong><br>
                    <img src="{{asset('img/indonesia-flag-1.png')}}" class="mr-1 ml-0"> Yogyakarta, Indonesia<br>
                </p>
                </div>
            </ul>
                
            </div>
            </div>
        </nav>
        
        <!-- Main content -->
        <div class="main-content">
            <!-- Section 1 Start -->
            <div class="section" id="start">
                <div class="pb-8 pt-5 pt-lg-8 d-flex" style="background-image: url({{asset('img/bg-gidicode.png')}}); min-height: 766px;  background-size: cover; background-position: center top;">
                    <div class="container-fluid">
                    <div class="mt-6 text-start" style="height:180px;">
                        <p class="title-start">Hi, we help you to create</p>
                        <div id="typed-strings" style="font-size:50pt;">
                            <p>Website for <strong>Business</strong></p>
                            <p>Website for <strong>Startup</strong></p>
                            <p>Website for <strong>Personal</strong></p>
                            <p>Apps for <strong>Education</strong></p>
                            <p>Mobile Apps</p>
                            <p>Web Apps</p>
                            <p>UI UX Design</p>
                            <p>Information System</p>
                            <p>Management System</p>
                        </div>
                        <span id="typed" class="typed-start"></span>
                    </div>
                    <div class="mt-3">
                        <a class="link-scroll" href="#connect-us" style="font-weight:800; font-size: 14pt;">
                            <span class="btn-inner--text">Connect Us</span>
                            <span class="btn-inner--icon ml-2"><i class="fas fa-arrow-right"></i></span>
                        </a>
                    </div>
                    </div>
                </div>
            </div>
            
            <div class="section" id="we-do">
                <div class="header pb-8 pt-5 pt-lg-8 d-flex" style="background-image: url(assets/img/bg-gidi-3.png); background-size: cover; background-position: left top;">
                    <div class="container-fluid">
                    <div class="content-mobile mt-5"></div>
                    <div class="text-we-do">
                        <p class="title-start" style="font-size: 18pt; line-height: 26pt">We do</p>
                        <div id="typed-strings-2">
                            <strong>something great</strong>
                            <strong>an innovation</strong>
                            <strong>a clear code</strong>
                            <strong>a clean design</strong>
                        </div>
                        <span id="typed-2" class="typed-we-do mt--3"></span>
                    </div>

                    <div class="mt-5"></div>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5">
                                <img src="./assets/img/it-business.png" style="width:100%" alt="">
                            </div>
                            <div class="col-md-7">
                            <h1>for business & startup</h1>
                            <p>We build a best quality of IT product include Web, Apps and API for your company and startup</p>
                            <a href="#for-biz" style="font-weight:700">
                                <span class="btn-inner--text">Explore</span>
                                <span class="btn-inner--icon ml-2"><i class="fas fa-arrow-right"></i></span>
                            </a>
                            </div>
                        </div>
                        </div>

                        <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5">
                                <img src="./assets/img/it-edu.png" style="width:100%" alt="">
                            </div>
                            <div class="col-md-7">
                            <h1>for education need</h1>
                            <p>Yes, we also do for education need. Improve your student skills with IT, we build an apps for institution, school or any educational institutions</p>
                            </div>
                        </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                            <div class="col-md-5">
                                <img src="./assets/img/uiux.png" style="width:100%" alt="">
                            </div>
                            <div class="col-md-7">
                                <h1>UI & UX design</h1>
                                <p>We really love clean design. we only code with the clear code and also clear design. Not only clean design, but easy to use and easy to understand</p>
                            </div>
                            </div>
                        </div>

                    </div>
                    </div>
                </div>
            </div>

            <div class="section" id="for-biz">
                <div class="header pb-8 pt-5 pt-lg-8 d-flex" style="background-image: url(assets/img/bg-gidi-4.png); background-size: cover; background-position: left top;">
                <div class="container-fluid">
                    <div class="content-mobile mt-5"></div>
                    <div class="text-we-do">
                    <p class="title-for-biz" style="font-size: 14pt;">improve your business or startup with</p>
                    <div id="typed-strings-3">
                        <strong>Website</strong>
                        <strong>Mobile Apps</strong>
                        <strong>Web Apps</strong>
                        <strong>Online Payment</strong>
                        <strong>Information System</strong>
                        <strong>Management System</strong>
                    </div>
                    <span id="typed-3" class="typed-we-do mt--3"></span>
                    </div>

                    <div class="mt-5 content-desktop"></div>
                    <div class="mt--3 content-mobile"></div>
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#offers" role="tab" aria-controls="pills-contact" aria-selected="false">Special Offers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#web" role="tab" aria-controls="pills-home" aria-selected="true">Website</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#apps" role="tab" aria-controls="pills-profile" aria-selected="false">Apps</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#si" role="tab" aria-controls="pills-contact" aria-selected="false">Information System</a>
                    </li>
                    </ul>
                    <div class="tab-content mt-5" id="pills-tab">
                    <div class="tab-pane fade show active" id="offers" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row">
                        <div class="col-md-6">
                            <span>build IT infrastuctur for your business or startup from*</span>
                            <p style="font-weight:800; font-size:32pt">IDR. 0,-</p>
                            <p style="font-size:8pt" class="mt--4">*term and condition applied</p>
                        </div>

                        <div class="col-md-6">
                            <div class="content-mobile mt-6"><hr></div>
                            <p class="mt--3" style="font-weight:700; font-size:24pt">tell us about your IT need for business, startup or even your great idea</p>
                            <a class="btn btn-icon bg-yellow" id="demo01" href="#animatedModal" style="color:#000">
                            <span class="btn-inner--icon mr-1"><i class="far fa-lightbulb"></i></span>
                            <span class="btn-inner--text">tell us here</span>
                            </a>
                        </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="web" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <p style="font-weight:700; font-size:24pt">improve your business professionalism and customers trust<br> <small>with an elegant website</small></p>
                        <br>
                        <div class="row">
                        <div class="col-md-4 mb-3">
                            <h3>Company Profile</h3>
                            <p>a website is something that can represent your company, that's why you should have a great website for your company</p>
                        </div>
                        <div class="col-md-4 mb-3">
                            <h3>e-Commerce Site</h3>
                            <p>e commerce website can be your storefront, even more than that can also be used for online transactions</p>
                        </div>
                        <div class="col-md-4 mb-3">
                            <h3>Online Payment</h3>
                            <p>your business will be more trusted if you have an online payment system especially with a credit card. We can integrate with several payment gateways to your site</p>
                        </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="apps" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <p style="font-weight:700; font-size:24pt">are you ready to face 4.0 industrial revolution?<br> <small>yes we're ready, and you also have to</small></p>

                        <br>
                        <div class="row">
                        <div class="col-md-4 mb-3">
                            <h3>Mobile Apps</h3>
                            <p>Android or iOS we always ready to do for your company</p>
                        </div>
                        <div class="col-md-4 mb-3">
                            <h3>Web Apps</h3>
                            <p>try to expand your market size, team collaboration, or other reason that need a web apps. We will do it for you</p>
                        </div>
                        <div class="col-md-4 mb-3">
                            <h3>Web API</h3>
                            <p>A Web API is an application programming interface for either a web server or a web browser</p>
                        </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="si" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <p style="font-weight:700; font-size:24pt"> a system to simplify your work and team<br> <small>yes you will need</small></p>
                        <br>  
                        <div class="row">
                        <div class="col-md-4 mb-3">
                            <h3>Point of Sales (POS)</h3>
                            <p>application that will make it easier for you to manage your finances and inventory</p>
                        </div>
                        <div class="col-md-4 mb-3">
                            <h3>Management System</h3>
                            <p>application that makes it easy to manage your business and team</p>
                        </div>
                        </div>
                    </div>
                    </div>
                
                </div>
                </div>
            </div>
            
            <div class="section" id="about-us">
                <div class="header pb-8 pt-5 pt-lg-8 d-flex" style="background-image: url(assets/img/bg-gidi-4.png); background-size: cover; background-position: left top;">
                <div class="container-fluid">
                    <div class="content-mobile mt-5"></div>
                    <p style="font-weight:700; font-size:24pt">CODE is not only our work,<br> it's our passion, it's our hobby</p>
                    <div class="text-we-do">
                    <div id="typed-strings-4">
                        <strong>Java</strong>
                        <strong>PHP</strong>
                        <strong>PHP Laravel</strong>
                        <strong>PHP Codeigniter</strong>
                        <strong>ReactJS</strong>
                        <strong>AngularJS</strong>
                        <strong>WordPress</strong>
                        <strong>UI Design</strong>
                        <strong>UX Analyst</strong>
                    </div>
                    <p class="title-for-biz" style="font-size: 14pt;">we are expert in <span id="typed-4" class="text-primary" style="font-weight:700"></span></p>
                    </div>
                    <div class="row mt--5">
                        <div class="col-md-4 mb-4">
                        <img src="./assets/img/team-gidi.png" style="width:100%" alt="">
                        </div>
                        <div class="col-md-6" id="about-us-scroll">
                        <p style="font-size:18pt; font-weight:600">gidicode is an innovation creation agency focused on technology</p>
                        <p>we started our first project in Yogyakarta to create a startup focused on tourism development it is <a href="https://gidsnesia.com" target="blank">Gidsnesia</a>. Then we continue to be trusted to develop an information system for the needs of <a href="https://uii.ac.id" target="blank">Universitas Islam Indonesia</a> in Yogyakarta. until now we have completed dozens of projects ranging from websites, applications and information systems</p>
                        <p>we currently have 7 people, consisting of 4 software developers and 3 business development people</p>
                        <p style="font-size:18pt; font-weight:600">gidicode is a large part of gidsnesia</p>
                        <p>gidsnesia is our first project, a local market guide and social media for tourists, focusing on the tourism sector. The Gidicode team is a large part of Gidsnesia. Where Gidsnesia has become one of the 100 best startups in <a href="https://startupistanbul.com" target="blank">Startup Istanbul 2018</a></p>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            
            <div class="section" id="connect-us">
                <div class="header pb-8 pt-5 pt-lg-8 d-flex" style="background-image: url(assets/img/bg-gidi-4.png); background-size: cover; background-position: left top;">
                    <div class="container-fluid">
                    <div class="content-mobile mt-5"></div>
                    <p style="font-weight:700; font-size:32pt">connect us</p>
                    
                    <div class="mt-6"></div>
                    <div class="row">
                        <div class="col-md-4 mb-4">
                            <strong>our main office</strong>
                            <p>Jln. Borobudur No.13, Klaten, Jawa Tengah 57416</p>
                            <strong>co working space</strong>
                            <p>Jogja Digital Valley - Jln. Kartini No.7, Terban, Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55223</p>
                            <strong>official email</strong>
                            <p><a href="mailto:hi@gidicode.com">hi@gidicode.com</a></p>
                            <strong>WhatsApp</strong>
                            <p><a href="https://wa.me/6281336723747">+62 813 36723747</a></p>
                        </div>
                        <div class="col-md-6" id="about-us-scroll">
                            <p style="font-size:18pt; font-weight:700">leave us a message</p>
                            <form action="old/kontak/sendmessage">
                            <div class="form-group">
                                <label>your name</label>
                                <input type="text" class="form-control" name="name" required>
                            </div>
                            <div class="form-group">
                                <label>your email</label>
                                <input type="text" class="form-control" name="email" required>
                            </div>
                            <div class="form-group">
                                <label>your mobile phone</label>
                                <input type="text" class="form-control" name="phone" required>
                            </div>
                            <div class="form-group">
                                <label>tell us something</label>
                                <textarea name="story" id="" cols="30" rows="10" class="form-control" required></textarea>
                            </div>
                            <button class="btn btn-primary float-right">send</button>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
                </div>

            <!-- Modal Biz Offers -->
            <!--DEMO01-->
            <div id="animatedModal">
                <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
                <div class="close-animatedModal">
                <div class="container-fluid mt-4">
                    <button class="btn btn-icon btn-sm btn-danger btn-close-modal float-right" onclick="location.reload();">
                        <span class="btn-inner--icon"><i class="far fa-times-circle"></i></span>
                        <span class="btn-inner--text">Close</span>
                    </button>
                </div> 
                </div>
                    
                <div class="modal-content" style="background-color:transparent; box-shadow:none">
                <div class="container mt-5">
                    <div class="row">
                    <div class="col-md-6 col-sm-8 mx-auto">
                        <p class="text-center" style="font-size:22pt; font-weight:700">tell us the story about your company or idea</p>
                        <hr>
                        <form action="old/kontak/sendidea" class="mb-6">
                        <div class="form-group">
                            <label for="">Your Name</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="">I Have...</label>
                            <select name="have" id="" class="form-control" required>
                            <option value="">Choose one</option>
                            <option value="Startup (Idea Stage)">Startup (Idea Stage)</option>
                            <option value="Startup (Prototype Stage)">Startup (Prototype Stage)</option>
                            <option value="Startup (User Stage)">Startup (User Stage)</option>
                            <option value="Startup (Paying User Stage)">Startup (Paying User Stage)</option>
                            <option value="company ( < 1 year experience">Company ( < 1 year experience)</option>
                            <option value="Company (1 - 2 years experience)">Company (1 - 2 years experience)</option>
                            <option value="Company ( > 2 years experience)">Company ( > 2 years experience)</option>
                            <option value="Great Idea">Great Idea</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Your Company / Idea</label>
                            <input type="text" class="form-control" name="company" required >
                        </div>
                        <div class="form-group">
                            <label for="">Your Company Website</label>
                            <input type="text" class="form-control" name="url_company" required >
                        </div>
                        <div class="form-group">
                            <label for="">Your Email</label>
                            <input type="text" class="form-control" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="">Your Mobile Phone</label>
                            <input type="text" class="form-control" name="phone">
                        </div>
                        <div class="form-group">
                            <label for="">Please share us your pitchdesk / company profile</label>
                            <input type="text" class="form-control" name="url_file" required>
                            <small class="text-warning">share us the url of your file from Google Drive, One Drive, etc.</small>
                        </div>
                        <div class="form-group">
                            <label for="">Tell us the story about it</label>
                            <textarea name="story" class="form-control" id="" cols="30" rows="10" required></textarea>
                        </div>

                        <button class="btn btn-primary form-control" type="submit">send to us</button>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>

        </div>
        
        <!-- Argon Scripts -->
        <!-- Core -->
        <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
        <script src=" {{ asset('vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }} "></script>
        <!-- Optional JS -->
        <script src="{{ asset('vendor/chart.js/dist/Chart.min.js') }}"></script>
        <script src="{{ asset('vendor/chart.js/dist/Chart.extension.js') }}"></script>
        <!-- Argon JS -->
        <script src="{{ asset('js/argon.js') }}"></script>

        <!-- Typewriter effect -->
        <script src="{{ asset('vendor/typed/typed.js') }}"></script>

        <!-- NicescrollJS-->
        <script src="{{ asset('vendor/nicescroll/jquery.nicescroll.min.js') }}"></script>

        <!-- Jquery Scroll Tabs -->
        <script src="{{ asset('vendor/jquery-scroll/jquery.scrolling-tabs.min.js') }}"></script>

        <!-- Modal Fullscreen -->
        <script src="{{ asset('vendor/animatedModal/animatedModal.min.js') }}"></script>

        <script>
            var typed = new Typed('#typed', {
            stringsElement: '#typed-strings',
            typeSpeed: 60,
            backSpeed: 10,
            smartBackspace: true, // this is a default
            loop: true
            });

            var typed2 = new Typed('#typed-2', {
            stringsElement: '#typed-strings-2',
            typeSpeed: 120,
            backSpeed: 10,
            smartBackspace: true, // this is a default
            loop: true
            });

            var typed3 = new Typed('#typed-3', {
            stringsElement: '#typed-strings-3',
            typeSpeed: 120,
            backSpeed: 10,
            smartBackspace: true, // this is a default
            loop: true
            });

            var typed4 = new Typed('#typed-4', {
            stringsElement: '#typed-strings-4',
            typeSpeed: 120,
            backSpeed: 10,
            smartBackspace: true, // this is a default
            loop: true
            });

            $("a.nav-link").click(function(){
            $(".navbar-collapse").removeClass("show");
            $(".navbar-collapse").addClass("hide");
            });


            $("a.link-scroll[href^='#']").on('click', function(e) {

            // prevent default anchor click behavior
            e.preventDefault();
            var target = $(this).attr('href');
            // animate
            $('html, body').animate({
                scrollTop: $(this.hash).offset().top
            }, 800, function(){

                // when done, add hash to url
                // (default click behaviour)
                window.location.hash = target;
            });
            });

            $('.nav-pills').scrollingTabs({
            bootstrapVersion: 4,
            cssClassLeftArrow: 'fa fa-chevron-left',
            cssClassRightArrow: 'fa fa-chevron-right'
            });

            $("#demo01").animatedModal({
            animatedIn:'bounceInUp',
            animatedOut:'bounceOutDown',
            color:'rgba(255,255,255,0.95)'
            });

            $(document).ready(function(){
            $(".btn-close-modal").click(function(){
                $("html").css({'overflow':'hidden'});
                $("body").css({'overflow':'hidden'});
            });
            });

            $("body").niceScroll();
            $("#about-us-scroll").niceScroll({
            autohidemode: false,
            cursorborderradius: '10px'
            });
        </script>
    </body>

</html>